package com.company;

/**
 * Created by julyr on 08/05/2017.
 */
public class Persona {
    public String varpublica;
    private String varprivate;
    final String varfinal="12";
    protected String varprotect;
    static String compartido="Hola";

    public static void main (String[] args){
        Persona persona = new Persona();
                persona.varpublica="variable publica";
                persona.varprivate="variable privada";

    }

    public String getVarprivate() {
        return varprivate;
    }

    public void setVarprivate(String varprivate) {
        this.varprivate = varprivate;
    }
}
